<?php
namespace Src;


class Solution {

    function addEdge(Vertex $a, Vertex $b) {
        $a->front->push($b);
        $b->addDemand(1);
        $a->addSupply(1);
        
    }

    private $vertex;
    function minNumberOfSemesters($n, $relations, $k) {

        
        for($i=1;$i<=$n;$i++) {
            $vertices[$i] = new Vertex($i);
        }

        $this->vertex = $vertices[4];
        foreach($relations as $relation) {
            $this->addEdge( $vertices[$relation[0]],  $vertices[$relation[1]]);
        }
        $readyList = new ReadyList(new Vertex($n+1));
        // foreach ( $vertices as $v) {
        //     if($v->outDegree > 0 ) $readyList->add($v);
        // }
        foreach ( $vertices as $v) {
            if($v->demand == 0 ) $readyList->add($v);
        }
        echo $readyList->print()." :: ".$this->vertex->demand."<br/>";
        return $this->recursiveB($readyList, $k);
        
    }
    
    function recursiveA(ReadyList $readyList, $n, $k) {
        $min = 99999;
        $pick = $readyList->head();
        if($readyList->head()->next) echo ": { ListHead:".$readyList->head()->id." }<br/>";
        $status = -1;
        while($pick = $pick->next ){
            if($pick->produced) continue;
            if($status == $pick->frontStatus()) continue;
            $status = $pick->frontStatus();
            $pick->produced = true;
            $readyList->available --;
            if($n != 1 && $readyList->available > 0 ) {
                $nextList = new ReadyList($readyList->origin(), $pick);
                $num = $this->recursiveA($nextList, $n-1, $k);
            }
            else {
                // $readyList->origin();m  
                
                $nextList = $readyList->prepare();
                echo $readyList->available."=".$nextList->print()." :: ".$this->vertex->demand."<br/>";
                $num = $this->recursiveB($nextList, $k);
                echo ": ".$num."<br/>";
            }
            if($num < $min) {
                $min = $num;
            }
            $pick->produced = false;
            $readyList->resetFront();
            $readyList->available++;
            if($readyList->distance($pick->id) < $n) break;
        };
        return  $min;
        
    }
    
    function recursiveB(ReadyList $readyList, $k){
        $product_num = $readyList->checkWell();
        if($product_num) {
            return ceil($product_num/$k);
        }
        else {
            return $this->recursiveA($readyList, $k, $k)+1;
        }
    }

    


}