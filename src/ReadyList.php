<?php
namespace Src;

class ReadyList{
    private $head;
    private $origin;
    private $preHead;
    private $tail;
    public $available = -1;
    function __construct(Vertex $origin, Vertex $head = null, Vertex $preHead = null) {
        $this->origin = $origin;
        if($head == null) $this->head = $origin;
        else $this->head = $head;
        $pick = $origin;
        do{
            $this->tail = $pick;
            if(!$pick->produced)$this->available ++;
        }while($pick = $pick->next);
        
    }
    
    
    public function origin() {
        return $this->origin;
    }
    
    public function reset() {
        $this->head = $this->origin;
    }
    public function add(Vertex $v) {
        $pick = $this->origin;
        while($pick = $pick->next){
            if($pick->id == $v->id) return ;
        }
        if($this->origin == null) $this->origin = $v;
        if($this->head == null) $this->head = $v;
        if($this->tail == null) $this->tail = $v;
        else {
            $this->tail->next = $v;
            $this->tail = $this->tail->next;
        }
        $this->available++;
    }
    public function head() {
        return $this->head;
    }
    
    public function checkWell() {
        $pick = $this->origin;
        $wellStack = new Stack();
        while($pick = $pick->next){
            if($pick->produced) continue;
            if($pick->outDegree != 0) return 0;
            $wellStack->push($pick);
        }
        $count = $wellStack->size();

        // while($pick = $wellStack->pop()) {
        //     $pick->resetDemand();
        // }
        return $count;
    }
    
    public function prepare() {
        $prepareList = $this->clone();
        $pick = $prepareList->origin();
        while($pick = $pick->next){
            if(!$pick->produced )  continue;
            while($p = $pick->front->pop()) {
                $p->demand --;
                if($p->demand == 0) {
                    $prepareList->add(clone $p);
                    $p->resetDemand();
                }
            }
        }
        return $prepareList;

    }
    
    public function clone(){
        $pick = $this->origin;
        $cloneOrigin = clone $this->origin;
        $clonePick = $cloneOrigin;
        while($pick = $pick->next) {
            $clonePick->next = clone $pick;
            $clonePick = $clonePick->next;
        }
        return new ReadyList($cloneOrigin);
    }
    public function print(){
        $str = "";
        $pick = $this->origin;
        while($pick = $pick->next){
            if($pick->produced) $str .="o";
            else $str .= "x";
            $str .= $pick->id."[".$pick->demand."] ";
        }
        return $str;
    }
    public function distance($vid) {
        $pick = $this->origin;
        $count = 0;
        $find = false;
        while($pick = $pick->next) {
            if($find && !$pick->produced)
                $count++;
            if($vid == $pick->id) $find = true;
        }
        return $count;
    }
    public function resetFront(){
        $pick = $this->origin;
        while($pick = $pick->next) {
            $pick->front->reset();
        }
    }
}