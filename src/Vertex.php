<?php
namespace Src;

use Src\Stack;

class Vertex {
    public $id;
    public $front;
    // public $level = 0;
    public $produced = false;
    // public $beside ;
    private $inDegree = 0 ;
    public $demand = 0 ;
    public $outDegree = 0 ;
    public $next = null;
    private $frontStatus = null;
    function __construct($id) {
        $this->id = $id;
        // $this->level = $level;
        $this->front = new Stack();
        // $this->back = new Stack();
    }
    public function resetDemand() {$this->demand = $this->inDegree; } 
    public function addDemand($c) { 
        $this->demand += $c;
        $this->inDegree = $this->demand;
    }
    public function addSupply($c) { 
        $this->outDegree += $c;
    }

    public function compareFront(Vertex $v) {
        $v->front->reset();
        while($pick = $v->front->pop() ) {
            $this->front->reset();
            while($p = $this->front->pop()) {
                if($pick->id == $p->id) return true;
            }
        }
        return false;
    }
    public function frontStatus(){
        if($this->frontStatus) return $this->frontStatus;

        $status =0;
        $this->front->reset();
        while($p = $this->front->pop()) {
            $status += pow(2, $p->id);
        }

        $this->front->reset();

        $this->frontStatus = $status;
        return $this->frontStatus;

    }
}