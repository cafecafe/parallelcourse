<?php
    namespace Src;
    use Src\Solution;
    class CalculatorController {
        public function __construct($method) {
            $this->requestMethod = $method;
        }
        
        public function post() {
            if($this->requestMethod != "POST") return;
            $input = json_decode(file_get_contents('php://input'), TRUE);
            $solution = new Solution();
            $anwser = $solution->minNumberOfSemesters($input["amount"], $input["relations"], $input["course"]);
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['body'] = json_encode([ "answer" => $anwser ]);
            if ($response['body']) {
                echo $response['body'];
            }


        }



    }