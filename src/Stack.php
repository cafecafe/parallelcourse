<?php
namespace Src;

use Src\Vertex;

class Stack {
    private $top=-1;
    private $space = array();
    private $point=-1;

    public function push(Vertex $v) {
        $this->space[] = $v;
        $this->top++;
        $this->point = $this->top;
    }
    public function pop() {
        if ($this->top < 0) return false;
        else return $this->space[$this->top--];
    }
    
    public function reset() {
        $this->top = $this->point;
    }
    public function size(){
        return $this->point+1;
    }
    public function getSpace() { return $this->space;}
}